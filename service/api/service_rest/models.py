from django.db import models
from django.urls import reverse

# Create your models here.
class AutoMobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    # color = models.CharField(max_length=50)
    # year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin



class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    def get_api_url(self):
        return reverse('api_show_technician', kwargs={"pk": self.id})



class Appointment(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
    date = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse('api_show_appointment', kwargs={"pk": self.id})
