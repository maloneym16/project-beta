from django.contrib import admin

# Register your models here.
from .models import AutoMobileVO, Technician, Appointment

@admin.register(AutoMobileVO)
class AutoMobileVOadmin(admin.ModelAdmin):
    pass

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    pass
