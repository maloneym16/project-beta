from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutoMobileVO, Technician, Appointment
from common.json import ModelEncoder
import json

# Create your views here.

# Technician Encoder
class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]


# Appointment Encoder
class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "reason",
        "date",
        "technician",
        "completed",
        "vip",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }


# LIST TECHNICIANS --- GET AND POST
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a technician."}
            )
            response.status_code = 400
            return response

# TECHNICIANS --- GET, DELETE
@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician doesn't exist"},
            )
            response.status_code = 400
            return response
    else:
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician has been successfully deleted"}
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician doesn't exist"})
            response.status_code = 400
            return response



# LIST APPOINTMENTS --- GET AND POST
@require_http_methods(["GET", "POST"])
def api_list_appointments(request, vin=None):
    if request.method == "GET":
        if vin == None:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder= AppointmentEncoder
            )
        else:
            try:
                appointments = Appointment.objects.filter(vin=vin)
                return JsonResponse(
                    {"appointments": appointments},
                    encoder=AppointmentEncoder
                )
            except Appointment.DoesNotExist:
                response = JsonResponse(
                    {"message": "Appointment doesn't exist"}
                )
                response.status_code = 400
                return response
    else:
        content = json.loads(request.body)
        try:
            technician_number = content["technician"]
            technician = Technician.objects.get(id=technician_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician doesn't exist"}
            )
        if AutoMobileVO.objects.filter(vin=content["vin"]).exists():
            content["vip"] = True
        else:
            content["vip"] = False
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )




@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment doesn't exist"}
            )
            response.status_code = 400
            return response
    else:
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment successfully deleted"}
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment doesn't exist"}
            )
            response.status_code = 400
            return response
