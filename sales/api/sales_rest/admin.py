from django.contrib import admin

# Register your models here.
from .models import AutoMobileVO, SalesPerson, SalesRecord, PotentialCustomer

@admin.register(AutoMobileVO)
class AutoMobileVOadmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesRecord)
class SalesRecordAdmin(admin.ModelAdmin):
    pass

@admin.register(PotentialCustomer)
class PotentialCustomerAdmin(admin.ModelAdmin):
    pass
