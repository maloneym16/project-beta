from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import SalesPerson, PotentialCustomer, SalesRecord, AutoMobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutoMobileVO
    properties = [
        "vin",
        "import_href",
        "id",
        "color",
        "year",
    ]
#salesperson encoder
class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

#customer encoder
class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_person",
        "customer",
        "price",
        "id",
        "import_href",

    ]
    encoders = {
        "potentialcustomer": PotentialCustomerEncoder(),
        "salesperson": SalesPersonEncoder(),
        "automobile": AutomobileVOEncoder(),
   }

#List of sales (GET, POST)
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder= SalesRecordEncoder
        )
    else: #Post
        try:
            content = json.loads(request.body)
            salesperson_id = content["salesperson_id"]
            salesperson = SalesPerson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
            potentialcustomer_id = content["potentialcustomer_id"]
            potentialcustomer = PotentialCustomer.objects.get(id=potentialcustomer_id)
            content["potentialcustomer"] = potentialcustomer
            automobile_id = content["automobile_id"]
            autmobile = AutoMobileVO.objects.get(id=automobile_id)
            content["automobile"] = automobile
            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder= SalesRecordEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a Sales Record."}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_sales(img, pk):
    if request.method== "DELETE":
        try:
            sale = SalesRecord.object.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                ecoder=SalesRecordEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sales Record Does Not Exist"})

#LIST OF CUSTOMERS (GET, POST, DELETE?)
@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        potentialcustomers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potentialcustomers": potentialcustomers},
            encoder= PotentialCustomerEncoder
        )
    else: #POST
        try:
            content = json.loads(request.body)
            potentialcustomer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                potentialcustomer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    if request.method == "DELETE":
        try:
            potentialcustomers = PotentialCustomer.objects.get(id=pk)
            potentialcustomers.delete()
            return JsonResponse(
                potentialcustomers,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST" ])
def api_salesman_list(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder= SalesPersonEncoder
        )
    else: #POST
        try:
            content = json.loads(requst.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    if request.method == "DELETE":
        try:
            salespersons = SalesPerson.objects.get(id=pk)
            salespersons.delete()
            return JsonResponse(
                salespersons,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist."})


@require_http_methods(["GET"]) #delete
def api_list_automobileVO(request):
    if request.method == "GET":
        automobileVOs = AutoMobileVO.objects.all()
        return JsonResponse(
            {"automobileVOs": automobileVOs},
            encoder=AutomobileVOEncoder,
        )

@require_http_methods(["DELETE"])
def api_delete_automobileVO(request, pk):
    if request.method == "DELETE":
            automobile = AutoMobileVO.objects.get(id=pk)
            automobile.delete()
            return JsonResponse(
                automobile,
                encoder=AutomobileVOEncoder,
                safe=False,
            )
