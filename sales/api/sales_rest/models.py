from django.db import models

class AutoMobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.vin}"


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return f"{self.name}"


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField()
    phone_number = models.CharField(max_length=20, unique=True)


class SalesRecord(models.Model):
    price = models.PositiveSmallIntegerField(null=True)
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_record",
        on_delete=models.PROTECT,
        null=True
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sales_record",
        on_delete=models.PROTECT,
    )
    automobile = models.ForeignKey(
        AutoMobileVO,
        related_name="sales_record",
        on_delete=models.PROTECT,
        null=True,
    )

    def __str__(self):
        return f"{self.customer} sales record"
