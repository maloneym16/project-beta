from django.urls import path
from .views import api_delete_automobileVO, api_delete_customer, api_delete_sales, api_delete_salesperson, api_list_automobileVO, api_list_customer, api_list_sales, api_salesman_list

urlpatterns =[
    path("auto/", api_list_automobileVO, name="api_list_automobileVO"),
    path("auto/<int:pk>", api_delete_automobileVO, name="api_delete_automobileVO"),
    path("salespersons/", api_salesman_list, name="api_salesman_list"),
    path("salespersons/<int:pk>", api_delete_salesperson, name="api_delete_salesperson"),
    path("customers/", api_list_customer, name="api_list_customer"),
    path("customers/<int:pk>", api_delete_customer, name="api_delete_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>", api_delete_sales, name="api_delete_sales")
]
