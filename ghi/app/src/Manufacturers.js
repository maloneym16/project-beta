import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';

const Manufacturers = () => {
    const [manufacturer, setManufacturer] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8100/api/manufacturers/")
            .then(response => response.json())
            .then(data => {
                setManufacturer(data.manufacturer);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const onDeleteManufacturerClick = (manufacturer) => {
        const manufacturerHref = manufacturer.href;
        const hrefComponents = manufacturer.href.split("/");
        const pk = hrefComponents[hrefComponents.length - 2];
        const manufacturerURL = `http://localhost:8100/api/manufacturers/${pk}`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            }
        }
        fetch(manufacturerURL, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.deleted) {
                    const currentManufacturer = [...manufacturer];
                    setManufacturer(currentManufacturer.filter(manufacturer => manufacturer.href !== manufacturerHref));
                }
            })
            .catch(e => console.log('error: ', e));
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturer.map(manufacturer => {
                        return (
                            <tr key={manufacturer.href}>
                                <td>
                                    <button onClick={() => onDeleteManufacturerClick(manufacturer)}>X</button>
                                    <span>{manufacturer.model}</span>
                                </td>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to={"/manufacturer/new"}>Create new manufacturer</Link>
        </>
    )
}


export default Manufacturers;
