import { useEffect, useState } from "react";

const ServiceHistory = () => {
    const [appointments, setAppointments] = useState([]);
	const [results, setResults] = useState([]);
	const [vin, setVin] = useState("");
	const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const fetchApptList = async () => {
            const appointmentURL = "http://localhost:8080/api/appointments/";
            const response = await fetch(appointmentURL)

            if (response.ok) {
                const data = await response.json()
                setAppointments(data.appointments)
            }
        }
        fetchApptList()
    }, [])

    const handleSearch = async (event) => {
        const searched = appointments.filter((appointment) => {
            appointment.vin.includes(vin)
        })
        setResults(searched);
        setSubmitted(true)
    }

    return (
        <div className='container'>
            <h1>Service History</h1>
            <div className="input-group mb-2">
                <input value={vin} onChange={(e) => setVin(e.target.value)} type="text"/>
                <button onClick={handleSearch} type="button" className="btn btn-outline-secondary">Search by VIN</button>
            </div>
            {results.length > 0 && (
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>VIP</th>
                        </tr>
                    </thead>
                    <tbody>
                        {results.map((result) => {
                            return (
                                <tr key={result.id}>
                                    <td>{result.customer_name}</td>
                                    <td>{result.vin}</td>
                                    <td>{result.reason}</td>
                                    <td>{new Date(result.date).toLocaleDateString("en-US")}</td>
                                    <td>{new Date(result.date).toLocaleDateString([], {
                                        hour: "2-digit",
                                        minute: "2-digit",})}</td>
                                    <td>{result.technician.name}</td>
                                    <td>{result.vip ? "Yes" : "No"}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
            <div className="error-message">
                {submitted && results.length === 0 && (
                    <div id="error-message">
                        The VIN you entered has no appointment history.
                    </div>
                )}
            </div>
        </div>
    )

}

export default ServiceHistory;
