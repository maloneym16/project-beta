import { Link } from 'react-router-dom';
import React from 'react';

class SalesPersonList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            SalespersonArray: [],
        };
    }
    async componentDidMount() {
        const url = 'http://localhost:8090/api/salesperson/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ SalespersonArray: data.salespersons });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className='container'>
                <h1>Salespeople</h1>
                <div className='d-grid gap-2 d-sm-flex justify-content-sm-left'>
                    <Link to="new" className='btn btn-outline-success btn-lg btn-sm px-4 gap-3'>Add a salesperson</Link>
                </div>
                <div>
                    <table className='table table-striped'>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Employee Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.SalespersonArray.map((salesperson) => {
                                return (
                                    <tr key={salesperson.employee_number}>
                                        <td>{salesperson.name}</td>
                                        <td>{salesperson.employee_number}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default SalesPersonList;
