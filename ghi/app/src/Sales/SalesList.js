import { Link } from 'react-router-dom';
import React from 'react';

class SalesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            SalesArray: [],
        };
    }
    async componentDidMount() {
        const url = 'http://localhost:8090/api/sales/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ SalesArray: data.sales });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className='container'>
                <h1>Sales List</h1>
                <div className='d-grid hap-2 d-sm-flex justify-content-sm-left'>
                    <Link to="new" className='btn-outline-success btn-lg btn-sm px-4 gap-3'> Add a sale </Link>
                </div>
                <div>
                    <table className='table table-striped'>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>VIN</th>
                                <th>Customer</th>
                                <th>Salesperson</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.SalesArray.map((sale) => {
                                return (
                                    <tr key={sale.id}>
                                        <td>${sale.price}</td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.customer.name}</td>
                                        <td>{sale.salesperson.name}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default SalesList;
