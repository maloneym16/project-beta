import React from 'react';
import { useNavigate } from 'react-router-dom';

function withExtras(Component) {
    return (props) => (
        <Component {...props} useNavigate={useNavigate()}/>
    );
}

class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            name: '',
            phone_number: '',
            address: '',
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePhone_numberChange = this.handlePhone_numberChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomerUrl = await response.json();
            console.log(newCustomerUrl);
            const cleared = {
                name: '',
                phone_number: '',
                address: '',
            };
            this.setState(cleared);
            this.props.useNavigate(`/customers`);
        }
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handlePhone_numberChange(event) {
        const value = event.target.value;
        this.setState({ phone_number: value });
    }
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({ address: value });
    }

    render () {
        return (
            <div className='row'>
                <div className='offset-3 col-6'>
                    <h1>Create a new Customer</h1>
                    <form onSubmit={this.handleSubmit} id='create-location-form'>
                        <div className='form-floating mb-3'>
                            <input value={this.state.name} onChange={this.handleNameChange} placeholder='Name' required type="text" name='name' id='name' className='form-control'/>
                            <label>Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={this.state.phone_number} onChange={this.handlePhone_numberChange} placeholder="Phone_number" required type="text" name="phone_number" id='phone_number'
                            className='form-control'/>
                            <label> Phone Number </label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={this.state.address} onChange={this.handleAddressChange} placeholder="Address" required type="text" name="address" id='address'
                            className='form-control'/>
                            <label>Address</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default withExtras(CustomerForm);
