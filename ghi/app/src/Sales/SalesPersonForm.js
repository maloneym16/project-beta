import React from 'react';
import { useNavigate } from 'react-router-dom';

function withExtras(Component) {
    return (props) => (
        <Component {...props} useNavigate={useNavigate()}/>
    );
}

class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            name: '',
            employee_number: '',
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployee_numberChange = this.handleEmployee_numberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const salespersonUrl = 'http://localhost:8090/api/salespersons/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            const newSalespersonUrl = await response.json();
            console.log(newSalespersonUrl);

            const cleared = {
                name: '',
                employee_number: '',
            };
            this.setState(cleared);
            this.props.useNavigate(`/salespersons`);
        }
    }
     handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
     }
     handleEmployee_numberChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value })
     }

    render () {
        return (
            <div className='row'>
                <div className='offset-3 col-6'>
                    <h1>Create a new Sales Person</h1>
                    <form onSubmit={this.handleSubmit} id='create-location-form'>
                        <div className='form-floating mb-3'>
                            <input value={this.state.name} onChange={this.handleNameChange} placeholder='Name' required type="text" name='name' id='name' className='form-control'/>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={this.state.employee_number} onChange={this.handleEmployee_numberChange} placeholder='Employee_Number' required type='text' name='employee_number' id='employee_number' className='form-control' />
                            <label>Employee Number</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default withExtras(SalesPersonForm);
