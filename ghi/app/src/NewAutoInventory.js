import React, { useEffect, useState } from 'react';

class NewAutoInventory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            Vin: '',
            color: '',
            year: '',
            model: '',
            manufacturers: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleChange = this.handleSubmit.bind(this);
    };


    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        const automobileURL = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(automobileURL, fetchConfig);
        if (response.ok) {
            this.setState({
                Vin: '',
                color: '',
                year: '',
                model: '',
                manufacturers: '',
            });
        }
    }
    handleChange(event) {
        const value = event.target.value;
        this.setState({
            Vin: value,
            color: value,
            year: value,
            model: value,
            manufacturers: value,
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1> Add a new Automobile </h1>
                            <form onSubmit={this.handleSubmit} id="create-automobile-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChange} value={this.state.Vin} placeholder="Vin" required type="text" name="Vin" id="Vin" className='form-control' />
                                    <label htmlFor="Vin">Vin</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                                    <label htmlFor='color'>Color</label>

                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChange} value={this.state.year} placeholder="year" required type="number" name="year" id="year" className="form-control"/>
                                    <label htmlFor='year'>Year</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChange} value={this.state.model} placeholder="model" required type="text" name="model" id="model" className="form-control"/>
                                    <label htmlFor='model'>Model</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleChange} value={this.state.manufacturer} require name ="manufacturer" id="manufacturer" className="form-select">
                                        <option value=""> Choose a manufacturer</option>
                                        {this.state.manufacturers.map(manufacturer => {return (<option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}
                                        </option>)})}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                           </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};
export default NewAutoInventory;
