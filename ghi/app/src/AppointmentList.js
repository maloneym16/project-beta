import { useEffect, useState } from "react";

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        const fetchApptList = async () => {
            const appointmentURL = "http://localhost:8080/api/appointments/";
            const response = await fetch(appointmentURL)

            if (response.ok) {
                const data = await response.json()
                setAppointments(data.appointments)
            }
        }
        fetchApptList()
    }, [])

    const deleteAppt = async (id) => {
        const appointmentURL = `http://localhost:8080/api/appointments/detail/${id}/`;
        const fetchConfig = {
            method: "delete",
			headers: {
				"Content-Type": "application/json",
			},
        };
        const response = await fetch(appointmentURL, fetchConfig)
        if (response.ok) {
            window.location.reload()
        }
    }

    return (
        <div className='container'>
            <h1>Appointment List</h1>
            <div>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>VIP</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.customer_name}</td>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{new Date(appointment.date).toLocaleDateString("en-US")}</td>
                                    <td>{new Date(appointment.date).toLocaleDateString([], {
                                        hour: "2-digit",
                                        minute: "2-digit",})}</td>
                                    <td>{appointment.technician.name}</td>
                                    <td>{appointment.vip ? "Yes" : "No"}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )

}

export default AppointmentList;
