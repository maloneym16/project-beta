import { useEffect, useState } from "react";

const TechnicianForm = () => {
    const [name, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState("");
    const [submitted, setSubmitted] = useState(false);

    const resetState = () => {
        setName("");
        setEmployeeNumber("");
        setSubmitted(true);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const employee_number = employeeNumber;
        const data = {name, employee_number};

        const technicianURL = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(technicianURL, fetchConfig);
        if (response.ok) {
            event.target.reset();
            resetState()
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1> Create a Technician </h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={(e) => setName(e.target.value)} required placeholder="Name" type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Technician Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={(e) => setEmployeeNumber(e.target.value)} required placeholder="Employee Number" type="number" name="employeeNumber" id="employeeNumber" className="form-control"/>
                                <label htmlFor="employeeNumber">Employee Number</label>
                            </div>
                            <div className="create-button">
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                        {submitted && (
						<div
							className="alert alert-success mb-0 p-4 mt-4"
							id="success-message">
							Appointment has been scheduled
						</div>
					)}

                    </div>
                </div>
            </div>
        </div>

    )

}

export default TechnicianForm;
