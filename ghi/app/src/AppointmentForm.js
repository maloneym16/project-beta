import { useEffect, useState } from "react";

const AppointmentForm = () => {
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [reason, setReason] = useState("");
    const [date, setDate] = useState("");
    const [technicians, setTechnicians] = useState([]);
    const [specificTechnician, setSpecificTechnician] = useState("");
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const fetchAppt = async () => {
            const technicianURL = "http://localhost:8080/api/technicians/";
            const response = await fetch(technicianURL)

            if(response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians)
            }
        }
        fetchAppt()
    }, [])

    const resetState = () => {
        setVin("");
        setCustomer("");
        setReason("");
        setDate("");
        setSpecificTechnician("");
        setSubmitted(true);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const customer_name = customer
        const technician = specificTechnician;
        const data = {vin, customer_name, reason, date, technician}

        const appointmentURL = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(appointmentURL, fetchConfig);
        if (response.ok) {
            event.target.reset();
            resetState();
        }
    };

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1> Create an Appointment </h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={(e) => setVin(e.target.value)} required placeholder="VIN" type="text" name="vin" id="vin" className="form-control"/>
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={(e) => setCustomer(e.target.value)} required placeholder="Customer Name" type="text" name="customer" id="customer" className="form-control"/>
                                <label htmlFor="customer">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={(e) => setReason(e.target.value)} required placeholder="Reason" type="text" name="reason" id="reason" className="form-control"/>
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={(e) => setDate(e.target.value)} required placeholder="Date and Time" type="datetime-local" name="date" id="date" className="form-control"/>
                                <label htmlFor="Date Time">Date and Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={(e) => setSpecificTechnician(e.target.value)} required  name="technician" id="technician" className="form-select">
                                    <option value="">
                                        Select a Tachnician
                                    </option>
                                    {technicians.map((technician) => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="create-button">
                                <button className="btn btn-primary">Create</button>
                            </div>
                        </form>
                        {submitted && (
						<div
							className="alert alert-success mb-0 p-4 mt-4"
							id="success-message">
							Appointment has been scheduled
						</div>
					)}

                    </div>
                </div>
            </div>
        </div>
    )

}

export default AppointmentForm;
