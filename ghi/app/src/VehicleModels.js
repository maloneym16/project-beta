import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';

const VehicleModel = () => {
    const [vehicle, setVehicle] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => {
                setVehicle(data.vehicle);
            })
            .catch(e => console.log('error: ', e));
    }, [])
    const onDeleteVehicleClick = (vehicle) => {
        const vehicleHref = vehicle.href;
        const hrefComponents = vehicle.href.split('/');
        const pk = hrefComponents[hrefComponents.length - 2];
        const vehicleURL = `http://localhost:8100/api/models/${pk}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            }
        }
        fetch(vehicleURL, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.deleted) {
                    const currentVehicle = [...vehicle];
                    setVehicle(currentVehicle.filter(vehicle => vehicle.href !== vehicleHref));
                }
            })
            .catch(e => console.log('error: ', e));
    }


    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicle?.map(vehicle => {
                        return (
                            <tr key={vehicle.href}>
                                <td>
                                    <button onClick={() => onDeleteVehicleClick(vehicle)}>X</button>
                                    <span>{vehicle.model}</span>
                                </td>
                                <td>{vehicle.name}</td>
                                <td>{vehicle.manufacturer}</td>
                                <td>
                                    <img className="vehicle-image" src={vehicle.picture_url} />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );

}



export default VehicleModel;
