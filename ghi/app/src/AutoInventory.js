import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';

const AutoInventory = () => {
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8100/api/automobiles/")
            .then(response => response.json())
            .then(data => {
                setAutomobiles(data.automobiles);
            })
            .catch(e => console.log('error: ', e));

}, [])

    const onDeleteAutomobileClick = (automobile) => {
        const automobileHref = automobile.href;
        const hrefComponents = automobile.href.split("/");
        const pk = hrefComponents[hrefComponents.length - 2];
        const automobileURL = `http://localhost:8100/api/automobiles/${pk}`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            }
        };
        fetch(automobileURL, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.deleted) {
                    const auto = [...automobiles];
                    setAutomobiles(auto.filter(auto => auto.href !== automobileHref));
                }
            })
            .catch(e => console.log('error: ', e));
    }


    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles?.map(automobile => {
                        return (
                            <tr key={automobile.id}>
                                <td>
                                    <button onClick={() => onDeleteAutomobileClick(automobile)}>X</button>
                                    <span>{automobile.model}</span>
                                </td>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}




export default AutoInventory;
