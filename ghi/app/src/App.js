import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutoInventory from './AutoInventory';
import Manufacturers from './Manufacturers'
import NewAutoInventory from './NewAutoInventory'
import NewManufcaturerForm from './NewManufacturerForm'
import NewVehicleModel from './NewVehicleModel'
import VehicleModels from './VehicleModels'
import CustomerForm from './Sales/CustomerForm';
import CustomerList from './Sales/CustomerList';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesPersonList from './Sales/SalesPersonList';
import SalesHistoryList from './Sales/SalesPersonHistory';
import SalesList from './Sales/SalesList';
import SalesForm from './Sales/SalesForm';
import AppointmentList from './AppointmentList'
import AppointmentForm from './AppointmentForm'
import TechnicianForm from './TechnicianForm'
import ServiceHistory from './ServiceHistory'



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path ="manufacturers/">
            <Route path="" element={<Manufacturers />}  />
            <Route path="new/" element={< NewManufcaturerForm/>}  />
          </Route>
          <Route path="models/">
            <Route path="" element={<VehicleModels />}  />
            <Route path="new/" element={<NewVehicleModel  />}  />
          </Route>
          <Route path="automobiles/">
            <Route path="" element={<AutoInventory />} />
            <Route path="new/" element={<NewAutoInventory  />} />
          </Route>
          <Route path="appointments/">
            <Route path="" element={<AppointmentList  />} />
            <Route path="new/" element={<AppointmentForm  />} />
            <Route path="history/" element={<ServiceHistory />} />
          </Route>
          <Route path="technicians/new/" element={< TechnicianForm />}/>
          <Route path="sales/">
            <Route path="" element={<SalesList />} />
            <Route path="history/" element={< SalesHistoryList />} />
            <Route path="new/" element={< SalesForm />} />
          </Route>
          <Route path="salespersons/">
            <Route path="" element={< SalesPersonList />} />
            <Route path="new/" element={< SalesPersonForm /> } />
          </Route>
          <Route path="customer/">
            <Route path="" element={< CustomerList />} />
            <Route path="new/" element={<CustomerForm /> } />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
